package com.venusource.app.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Helloworld {
	@GetMapping("/")
	private String hello(){
		return "Hello World!";
	}
}
